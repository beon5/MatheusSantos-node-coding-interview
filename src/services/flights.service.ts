import { FlightsModel } from '../models/flights.model'
import { Person } from '../models/persons.model'

export class FlightsService {
    getAll() {
        return FlightsModel.find()
    }

    async addPassengers(flightCode: string, passengers: Person[]): Promise<void> {
        await FlightsModel.updateOne({ code: flightCode }, { passengers })
    }
}
