import { JsonController, Get, Patch, Body, Param } from 'routing-controllers'
import { Person } from '../models/persons.model'
import { FlightsService } from '../services/flights.service'

const flightsService = new FlightsService()

@JsonController('/flights', { transformResponse: false })
export default class FlightsController {
    @Get()
    async getAll() {
        const data = await flightsService.getAll()
        return {
            status: 200,
            data
        }
    }

    @Patch('/:code')
    async addPassengers(@Param('code') code: string, @Body() passengers: Person[]) {
        await flightsService.addPassengers(code, passengers)
        return "succesfully updated"
    }
}
